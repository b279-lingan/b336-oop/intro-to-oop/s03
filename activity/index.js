// Quiz 2:
// 1. Should class methods be included in the class constructor?
// No
// 2. Can class methods be separated by commas?
// No
// 3. Can we update an object’s properties via dot notation?
// Yes
// 4. What do you call the methods used to regulate access to an object’s properties?
// Accessor Methods
// 5. What does a method need to return in order for it to be chainable?
// Chainable methods

// =======================================================================

// Function Coding 2
// 1. Modify the Student class to allow the willPass() and willPassWithHonors() methods to be chainable. Hint - new properties may have to be introduced in the constructor.

class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;

        if (grades.length === 4) {
            if (grades.every(grade => grade >= 0 && grade <= 100)) {
                this.grades = grades;
            }
        } else {
            this.grades = undefined
        }

        this.gradeAve = undefined;
        this.pass = undefined;
        this.passWithHonor = undefined;
    }

    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        // return sum/4;
        this.gradeAve = sum / 4;
        return this;
    }

    willPass() {
        this.pass = this.computeAve() >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
        this.passWithHonor = (this.willPass() && this.computeAve() >= 90) ? true : false;
        return this;
    }
}
let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);